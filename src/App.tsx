import React, { Component } from 'react';
import styled from 'styled-components';

import Header from './components/header';
import Camera from './components/camera';
import Output from './components/output';

interface IAppState {
  cameraStream?: MediaStream
};

const AppContainer = styled.div`
  margin: 0 auto;
  min-width: 600px;
  max-width: 800px;
  width: 80%;

  @media screen
    and (orientation: portrait)
    and (max-width: 600px) {
    width: 100%;
  }
`;

const OutputContainer = styled.div`
  position: relative;
  grid-column: 1 / span 2;
`;

const InputContainer = styled.div``;

const Main = styled.main`
  display: grid;
  grid-template-columns: 2;
  grid-auto-flow: row;

  @media screen
    and (orientation: portrait)
    and (max-width: 600px) {
    column-count: 1;
  }
`;

class App extends Component<IAppState> {
  state = {
    cameraStream: undefined
  };

  handleCameraReady = (cameraStream: MediaStream) => this.setState({
    cameraStream
  });

  render() {
    const dimensions = {
      width: 600,
      height: 500
    };

    return (
      <AppContainer>
        <Header title="Juggler">
          <a href="https://bitbucket.org/reyawn/juggler">Bitbucket Link</a>
        </Header>
        <Main>
          <Camera {...dimensions} onReady={this.handleCameraReady} />
          <OutputContainer>
            <Output {...dimensions} inputStream={this.state.cameraStream} />
          </OutputContainer>
          <InputContainer>
            
          </InputContainer>
        </Main>
      </AppContainer>
    );
  }
}

export default App;
