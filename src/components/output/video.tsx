import React, { PureComponent } from 'react';
import styled from 'styled-components';

interface IVideoProps {
  inputStream?: MediaStream,
  onReady: (video: HTMLVideoElement) => void
};

const StyledVideo = styled.video`
  transform: scaleX(-1);
`;

export default class Video extends PureComponent<IVideoProps> {
  private videoElement?: HTMLVideoElement;

  private handleMetadata = () => {
    if (!this.videoElement) {
      return;
    }
    this.props.onReady(this.videoElement);
  }

  private setVideoElementRef = (element: HTMLVideoElement) => {
    const { inputStream } = this.props;
    if (inputStream) {
      element.srcObject = inputStream;
    }
    this.videoElement = element;
  }

  public render() {
    return <StyledVideo
      onLoadedMetadata={this.handleMetadata}
      playsInline
      ref={this.setVideoElementRef}
    />;
  }
}