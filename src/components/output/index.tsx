import React, { PureComponent } from 'react';
import styled from 'styled-components';

import * as posenet from '@tensorflow-models/posenet';
import Video from './video';

interface IOutputProps {
  inputStream?: MediaStream
  width: number,
  height: number
};

interface IOutputState {
  detectionStarted?: boolean,
  imageScaleFactor: number,
  minPartConfidence: number,
  minPoseConfidence: number,
  net?: posenet.PoseNet,
  outputStride: number,
  videoElement?: HTMLVideoElement
}

const Overlay = styled.canvas`
  position: absolute;
  top: 0;
  left: 0;
`;

export default class Output extends PureComponent<IOutputProps, IOutputState> {
  state = {
    detectionStarted: false,
    imageScaleFactor: 0.5,
    minPartConfidence: 0.7,
    minPoseConfidence: 0.2,
    net: undefined,
    outputStride: 16,
    videoElement: undefined
  };

  overlayRef = React.createRef<HTMLCanvasElement>();

  public async componentDidMount() {
    this.setState({
      net: await posenet.load(0.75)
    });
  }

  public componentDidUpdate(prevProps: IOutputProps, prevState: IOutputState) {
    const {
      detectionStarted,
      net,
      videoElement
    } = this.state;
    if (videoElement && prevState.videoElement !== videoElement) {
      videoElement.play();
    }
    if (videoElement && net && !detectionStarted) {
      this.startDetection();
    }
  }

  private startDetection() {
    const {
      detectionStarted,
      imageScaleFactor,
      minPartConfidence,
      minPoseConfidence,
      net,
      outputStride,
      videoElement
    } = this.state;
    const canvas = this.overlayRef.current;

    const {
      width,
      height
    } = this.props;

    if (detectionStarted) {
      console.warn('detection already staretd');
      return;
    }

    if (!(canvas && videoElement && net)) {
      console.error('failed to start detection');
      return;
    }

    this.setState({
      detectionStarted: true
    });

    const ctx = canvas.getContext('2d');

    videoElement.width = canvas.width = width;
    videoElement.height = canvas.height = height;

    // since images are being fed from a webcam
    const flipHorizontal = true;

    const detectionFrame = async () => {
      if (!(canvas && videoElement && net)) {
        console.error('error during detection');
        return;
      }

      const {score, keypoints} = await net.estimateSinglePose(
        videoElement, imageScaleFactor, flipHorizontal, outputStride);

      ctx.clearRect(0, 0, width, height);
      if (score >= minPoseConfidence) {
        this.drawKeypoints(keypoints, minPartConfidence, ctx);
      }

      requestAnimationFrame(detectionFrame);
    }

    detectionFrame();
  }

  drawKeypoints(keypoints: [posenet.Keypoint], minConfidence: number, ctx: CanvasRenderingContext2D, scale = 1) {
    for (let i = 0; i < keypoints.length; i++) {
      const keypoint = keypoints[i];

      if (keypoint.score < minConfidence) {
        continue;
      }

      const {y, x} = keypoint.position;
      this.drawPoint(ctx, y * scale, x * scale, 3, 'red');
    }
  }

  drawPoint(ctx: CanvasRenderingContext2D, y: number, x: number, r: number, color: string) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, 2 * Math.PI);
    ctx.fillStyle = color;
    ctx.fill();
  }

  private handleVideoReady = (videoElement: HTMLVideoElement) => this.setState({
    videoElement
  });

  public render() {
    const { inputStream } = this.props;

    if (!inputStream) {
      return <div>Waiting for camera...</div>;
    }

    return <>
      <Video
        inputStream={inputStream}
        onReady={this.handleVideoReady}
      />
      <Overlay ref={this.overlayRef} />
    </>;
  }
}