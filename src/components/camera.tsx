import { PureComponent } from 'react';

interface ICameraProps {
  facingMode?: MediaTrackConstraintSet["facingMode"],
  width: MediaTrackConstraintSet["width"],
  height: MediaTrackConstraintSet["height"],
  onReady: (stream: MediaStream) => void
};

export default class Camera extends PureComponent<ICameraProps> {
  public async componentDidMount() {
    const {
      facingMode = 'user',
      width,
      height,
      onReady
    } = this.props;

    const stream = await navigator.mediaDevices.getUserMedia({
      audio: false,
      video: {
        facingMode,
        width,
        height
      }
    });

    onReady(stream);
  }
  
  public render() {
    return null;
  }
}