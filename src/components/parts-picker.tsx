import React, { PureComponent } from 'react';
import styled from 'styled-components';
import EmojiPicker from 'emoji-picker-react';

type SelectedPart = {
  name: string,
  code: string
}

interface IPartsPickerState {
  selectedParts: SelectedPart[]
}

interface IPartsPickerProps {
  onPartsSelected: (selectedParts: SelectedPart[]) => void
};

const partsInfo = {
  nose: 'nose',
  leftEye: 'left eye',
  rightEye: 'right eye',
  leftEar: 'left ear',
  rightEar: 'right ear',
  leftShoulder: 'left shoulder',
  rightShoulder: 'right shoulder',
  leftElbow: 'left elbow',
  rightElbow: 'right elbow',
  leftWrist: 'left wrist',
  rightWrist: 'right wrist',
  leftHip: 'left hip',
  rightHip: 'right hip',
  leftKnee: 'left knee',
  rightKnee: 'right knee',
  leftAnkle: 'left ankle',
  rightAnkle: 'right ankle'
};

const Emoji = styled.div`
`

const Part = ({ partName, code }) => (
  <div>{partName}</div>
)

export default class PartsPicker extends PureComponent<IPartsPickerProps, IPartsPickerState> {
  render() {
    return <EmojiPicker />;
  }
}