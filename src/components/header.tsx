import React from 'react';
import styled from 'styled-components';

interface IHeaderProps {
  title: string
};

const Title = styled.h1`
  display: inline-block;
  font-size: 1.3rem;
  margin: 0 .5rem 0 0;
  padding: 0.5rem;
  background-color: lightgray;
`;

const Header: React.FC<IHeaderProps> = ({children, title}) => (
  <header>
    <Title>{title}</Title>
    {children}
  </header>
);

export default Header;