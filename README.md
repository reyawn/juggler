## Programming Challenge

### Part 1

Write a function addN with the following behavior:

```
const add8 = addN(8);
add8(7); // resolves to 15
add8(100); // 108
```

This is a simple exercise of "currying".  A base case would be a function with two arguments that are added together and returned:

```
function add2(a, b) {
  return a + b;
}
```

Currying converts a function with multiple arguments into multiple functions of a single argument.  A curried version of the above could look like this:

```
const add = function (a) {
  return function (b) {
    return a + b;
  }
}
```

Or in ES6 (arrow functions!):

```
const add = (a) => (b) => a + b;
```

Curryings allows breaking down a function into composable, reusable pieces.  This can often be useful in EventEmitter patterns and other common programming use cases.

### Part 2

This was a project that a intended to have a bit of fun with and experiment with structure and layout, as well as with the very latest versions of React, Typescript, and other related components (a luxury not usually afforded with a day job!).  To save time on initial project scaffolding, I used [create-react-app](https://github.com/facebook/create-react-app).

I realized quickly that the challenge was mostly just going to be time-consuming.  The core logic around the tensorflow.js setup already exists and there are good examples (with lovely Apache 2.0 licenses).  The big items were getting the application working, setting up the UI, ironing out any issues, and getting the Emoji picker working (and the plumbing around the images).

Unfortunately I realized late on that the Emoji picker would be a bit of work, even though I did find some handy libraries.  Generating a grid of per-body-part items isn't challenging, but generating a grid of items with little sleep is a different thing.  Thus in the end I had to hang up my coat since I have a day job that demands my time and attention.

Running code can be found on my [bitbucket page](https://reyawn.bitbucket.io/).

Fun project, thanks!